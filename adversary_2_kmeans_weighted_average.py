"""
This scripts represents the 2nd adversary. It uses the k-means unsupervised learning algorithm to create 2 clusters,
namely in- and out- of the target model's training dataset, for conducting Membership Inference Attacks(MIAs).
The program shown below tries a number of random_state values for the k-means model and selects the highest ones.
Each record/line in the train/testing .txts include the confidence scores of the 10 classes of the CIFAR-10 dataset
and the ground truth label indicating if that specific record was included (0) or not (1) in the target model's training
dataset. The success rates are reported on a hold-out testing dataset whose size equals to the 20% of the training
dataset's size. For each different input_samples_no value both the training and the testing datasets are composed of
randomly selected records.
"""

from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

def main():

    dataset = pd.read_csv("adversary2_files/train.txt")
    dataset_test = pd.read_csv("adversary2_files/test.txt")

    #Changing pandas dataframe to numpy array
    X_data = dataset.iloc[:,1:11].values
    y_data = dataset.iloc[:,11].values

    percentages = []

    input_samples_no = [4000, 5000, 6000, 10000, 25000, 35000, 45000, 50000]

    print("Welcome to adversary 2 described in the paper \"MIAs on Well-Generalizable ML Models.\"")
    print("How many times do you want to run each experiment (8 in total) to collect the average MIAs' success rates?")
    val = input("Please provide as input ONLY an integer: ")
    times_to_perform_each_experiment = int(val)

    for i in range(len(input_samples_no)):
        print("Calculating the average MIA success rates for " +str(input_samples_no[i])+" input samples volume...")

        temp_percentages = []
        for j in range(times_to_perform_each_experiment):

            np.random.shuffle(X_data)
            np.random.shuffle(y_data)

            X = X_data[:input_samples_no[i]]
            y = y_data[:input_samples_no[i]]

            X_test = dataset_test.iloc[:,1:11].values
            y_test = dataset_test.iloc[:,11].values

            test_seq = int(input_samples_no[i]*20/100)
            np.random.shuffle(X_test)
            np.random.shuffle(y_test)
            X_test=X_test[:test_seq]
            y_test=y_test[:test_seq]

            # normalize the data
            sc = StandardScaler()
            X = sc.fit_transform(X)
            X_test = sc.fit_transform(X_test)

            max_success=0
            max_random_state=0
            for z in range(300):

                kmeans = KMeans(n_clusters=2, random_state=z).fit(X)
                y_pred_kmeans = kmeans.predict(X_test)

                correctly_classified = 0
                for label in range(len(y_pred_kmeans)):

                    if y_pred_kmeans[label] == y_test[label]:

                        correctly_classified = correctly_classified + 1

                mia_success = 100 * correctly_classified / len(y_pred_kmeans)
                if mia_success>max_success:
                    max_success=mia_success
                    max_random_state=j

            #print("For "+str(input_samples_no[i])+" the optimal random state is: "+str(max_random_state))
            temp_percentages.append(max_success)

        percentages.append(temp_percentages)
        avg_for_epoch=0
        for perce in temp_percentages:
            avg_for_epoch=avg_for_epoch+perce
        avg_for_epoch=avg_for_epoch/times_to_perform_each_experiment
        print("The average MIA success rate for input samples volume "+str(input_samples_no[i])+" is: "+str(round(avg_for_epoch,2))+"%")
        print()

    # produce the graph
    box_plot_data = [percentages[0], percentages[1], percentages[2], percentages[3], percentages[4], percentages[5],
                     percentages[6],percentages[7]]
    plt.title("Input Samples Volume vs. Inference Accuracy")
    plt.ylabel("Inference Accuracy (%)")
    plt.xlabel("Input Samples Volume")
    plt.boxplot(box_plot_data,labels=['4000', '5000', '6000', '10000', '25000', '35000', '45000', '50000'])
    plt.show()

    print("Thank you for viewing our code! :) ")


#execute program
main()