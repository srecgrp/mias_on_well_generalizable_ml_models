# Membership Inference Atttacks (MIAs) on Well-Generalizable Machine Learning (ML) Models #

### Description ###

* This repo contains the code for the reproduction of the two adversaries defined in the "MIAs on Well-generalizable ML models" paper. The project is written in Python 3.6.
* Clone and use the exact folder structure as described in this repo.

### How do I set it up? ###

1. Clone this repo. 
2. Install the dependencies: keras, tensorflow, sklearn, numpy, matplotlib, and pandas.
3. Once you set it up, issue: 
	* ```python adversary_1_MLP.py``` for adverasry 1. 
	* ```python adversary_2_kmeans.py``` for adverasry 2. 

### What about the dataset format? ###

* The dataset used is the CIFAR-10 [[1]](#1). However, as stated in the paper, the datasets for the two adversaries are the output posteriors of a target CNN model for each input sample (i.e., image) included in the CIFAR-10 dataset. In fact, the .txt files included in adversary1_files and adversary2_files folders already contain the confidence score for each class (10 in total) along wit the ground truth label about the membersip (0) or not (1) in the target model's **training dataset** for each input sample.  
* For both adversaries the testing dataset's size equals to the 20% of the respective size of the training dataset.

### Contribution guidelines ###

* Extend the MIAs.
* Code review.

### Who do I talk to? ###

* Directly to me! Contact me through email: adiony01@cs.ucy.ac.cy

## References
<a id="1">[1]</a> 
Krizhevsky, A., & Hinton, G. (2009). Learning multiple layers of features from tiny images.