"""Se touto to script edokimasa na exw ena train je test txts je apla kathe fora
na piannw ttosa samples oso en to leaked volume"""

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
import keras
from keras.models import Sequential
from keras.layers import Dense
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import os


def main():


    dataset = pd.read_csv("adversary1_files/train.txt")
    dataset_test = pd.read_csv("adversary1_files/test.txt")

    percentages = []
    leaked_dataset_volume = [5000, 10000, 15000, 20000, 28000, 36000, 44000, 50000]

    print("Welcome to adversary 1 described in the paper \"MIAs on Well-Generalizable ML Models.\"")
    print("How many times do you want to run each experiment (8 in total) to collect the average MIAs' success rates?")
    val = input("Please provide as input ONLY an integer: ")
    times_to_perform_each_experiment = int(val)

    for i in range(len(leaked_dataset_volume)):
        print("Calculating the average MIA success rates for " +str(leaked_dataset_volume[i])+" leaked data volume...")

        temp_percentages =[]
        for j in range(times_to_perform_each_experiment):

            # Changing pandas dataframe to numpy array
            X = dataset.iloc[:, 1:11].values
            y = dataset.iloc[:, 11].values

            np.random.shuffle(X)
            np.random.shuffle(y)

            X = X[:leaked_dataset_volume[i]]
            y = y[:leaked_dataset_volume[i]]

            #exclude specific lines
            X_test = dataset_test.iloc[:, 1:11].values
            y_test = dataset_test.iloc[:, 11].values
            test_range = int(leaked_dataset_volume[i]*20/100)

            np.random.shuffle(X_test)
            np.random.shuffle(y_test)

            X_test=X_test[:test_range]
            y_test = y_test[:test_range]

            # Normalizing the data
            sc = StandardScaler()
            X = sc.fit_transform(X)
            X_test = sc.fit_transform(X_test)

            ohe = OneHotEncoder()
            y = y.reshape(-1, 1)
            y_test = y_test.reshape(-1, 1)

            y = ohe.fit_transform(y).toarray()
            y_test = ohe.fit_transform(y_test).toarray()

            #define the neural network
            model = Sequential()
            model.add(Dense(256, input_dim=10, activation='relu'))
            model.add(Dense(128, activation='relu'))
            model.add(Dense(2, activation='softmax'))

            #define the optimizer
            sgd = keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False)

            #compile the model
            model.compile(loss='binary_crossentropy', optimizer=sgd, metrics=['accuracy'])

            # train the model on the train data
            history = model.fit(X, y, validation_data=(X_test, y_test), epochs=100, batch_size=64,verbose=0)

            #evaluate the performance of the model on the test datat
            y_pred = model.predict(X_test)
            # Converting predictions to label
            pred = list()
            for z in range(len(y_pred)):
                pred.append(np.argmax(y_pred[z]))
            # Converting one hot encoded test label to label
            test = list()
            for z in range(len(y_test)):
                test.append(np.argmax(y_test[z]))

            a = accuracy_score(pred, test)
            a = a * 100
            #print('Accuracy is:', a)
            temp_percentages.append((round(a,2)))

        percentages.append(temp_percentages)
        avg_for_epoch = 0
        for perce in temp_percentages:
            avg_for_epoch = avg_for_epoch + perce
        avg_for_epoch = avg_for_epoch / times_to_perform_each_experiment
        print("The average MIA success rate for the leaked dataset volume " + str(leaked_dataset_volume[i]) + " is: " + str(
            round(avg_for_epoch, 2)) + "%")
        print()

    # produce the graph
    box_plot_data = [percentages[0], percentages[1], percentages[2], percentages[3], percentages[4], percentages[5],
                     percentages[6],percentages[7]]
    plt.title("Leaked Dataset Volume vs. Inference Accuracy")
    plt.ylabel("Inference Accuracy (%)")
    plt.xlabel("Leaked Dataset Volume")
    #plt.boxplot(box_plot_data, labels=['16000', '20000', '24000', '28000', '36000', '44000', '50000'])
    plt.boxplot(box_plot_data, labels=['5000', '10000','15000', '20000', '28000', '36000', '44000', '50000'])
    plt.show()

    """
    # produce the graph
    box_plot_data = [percentages[0],percentages[1],percentages[2],percentages[3],percentages[4],percentages[5],percentages[6]]
    plt.title("Leaked Dataset Volume vs. Inference Accuracy")
    plt.ylabel("Inference Accuracy (%)")
    plt.xlabel("Leaked Dataset Volume")
    plt.boxplot(box_plot_data)
    plt.show()
    """

    print("Thank you for viewing our code! :) ")

#execute program
main()